import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor() {}
  cities: {country:string; name:string; desc:string; population:number; area:string; url:string ;votes:number; fav:boolean; buttonColor : string}[] = 
  [
    {
      country: "Slovakia",
      name: "Bratislava",
      desc: "Bratislava is the capital of Slovakia.",
      population: 430000,
      area: "367,6 km2",
      url: "http://sacr3-files.s3-website-eu-west-1.amazonaws.com/_processed_/csm_Bratislavsky%2520hrad_94d492b936.jpg",
      votes:0,
      fav: false,
      buttonColor : "#666"
    },
  
    {
      country: "Australia",
      name: "Canberra",
      desc: "Canberra is the capital of Australia.",
      population: 452497,
      area: "814 km2",
      url: "https://www.mynrma.com.au/-/media/local-guides/canberra/mobile-banner-aerial-view-canberra.jpg",
      votes:0,
      fav: false,
      buttonColor : "#666"
    },

    {
      country: "Iceland",
      name: "Reykjavik",
      desc: "Reykjavik is the capital of Iceland.",
      population: 129840,
      area: "273 km2",
      url: "https://www.fodors.com/assets/destinations/712533/downtown-cityscape-reykjavik-iceland_980x650.jpg",
      votes:0,
      fav: false,
      buttonColor : "#666"
    }
  ]; 

  vote(i)
  {
    if(this.cities[i].votes < 10)
    {
      this.cities[i].votes++;
    }
    else
    {
      alert("Max voted rached for this city!");
    }
  }

  reset(i)
  {
    this.cities[i].votes = 0;
    this.cities[i].fav=false;
    this.cities[i].buttonColor="#666";
  }
  
  favourite(i){
    for(let j = 0; j < this.cities.length; j++)
    {
      if(j == i)
      {
        this.cities[j].fav = true;
      }
      else{
        this.cities[j].fav = false;       
      }

    }

    for(let j = 0; j < this.cities.length; j++)
    {
      if(this.cities[j].fav == true)
      {
        this.cities[j].buttonColor = "red";
      }
      else{
        this.cities[j].buttonColor = "#666";
      }
    }
    
  }


}
